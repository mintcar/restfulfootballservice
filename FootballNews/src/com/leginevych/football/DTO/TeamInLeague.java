package com.leginevych.football.DTO;

public class TeamInLeague {

	private int id;
	private int position;
	private String nameTeam;
	private int games;
	private int wins;
	private int draws;
	private int losses;
	private int goals_scored;
	private int goals_conceded;
	private int goals_difference;
	private int points;
	
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getNameTeam() {
		return nameTeam;
	}
	public void setNameTeam(String nameTeam) {
		this.nameTeam = nameTeam;
	}
	public int getGames() {
		return games;
	}
	public void setGames(int games) {
		this.games = games;
	}
	public int getWins() {
		return wins;
	}
	public void setWins(int wins) {
		this.wins = wins;
	}
	public int getDraws() {
		return draws;
	}
	public void setDraws(int draws) {
		this.draws = draws;
	}
	public int getLosses() {
		return losses;
	}
	public void setLosses(int losses) {
		this.losses = losses;
	}
	public int getGoals_scored() {
		return goals_scored;
	}
	public void setGoals_scored(int goals_scored) {
		this.goals_scored = goals_scored;
	}
	public int getGoals_conceded() {
		return goals_conceded;
	}
	public void setGoals_conceded(int goals_conceded) {
		this.goals_conceded = goals_conceded;
	}
	public int getGoals_difference() {
		return goals_difference;
	}
	public void setGoals_difference(int goals_difference) {
		this.goals_difference = goals_difference;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


}
