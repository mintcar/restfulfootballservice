package com.leginevych.football.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.leginevych.football.DTO.TeamInLeague;
import com.leginevych.football.DTO.Team;

public class DataBaseManager {

	
	public ArrayList<TeamInLeague> GetLeague(Connection connection) throws Exception
	{
		ArrayList<TeamInLeague> teamData = new ArrayList<TeamInLeague>();
		try
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM barclayspremierleague");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				TeamInLeague teamInLeagueObject = new TeamInLeague();
				teamInLeagueObject.setId(rs.getInt("idBarclaysPremierLeague"));
				teamInLeagueObject.setPosition(rs.getInt("position"));
				teamInLeagueObject.setNameTeam(rs.getString("name"));
				teamInLeagueObject.setGames(rs.getInt("games"));
				teamInLeagueObject.setWins(rs.getInt("wins"));
				teamInLeagueObject.setDraws(rs.getInt("draws"));
				teamInLeagueObject.setLosses(rs.getInt("losses"));
				teamInLeagueObject.setGoals_scored(rs.getInt("goals_scored"));
				teamInLeagueObject.setGoals_conceded(rs.getInt("goals_conceded"));
				teamInLeagueObject.setGoals_difference(rs.getInt("goal_difference"));
				teamInLeagueObject.setPoints(rs.getInt("points"));
				
				teamData.add(teamInLeagueObject);
			}
			return teamData;
		}
		catch(Exception e)
		{
			System.out.println("Check DataBaseManager in GetLeague");
			throw e;
		}
	}
	public ArrayList<Team> GetTeam(Connection connection, String nameTeam) throws Exception
	{
		ArrayList<Team> teamData = new ArrayList<Team>();
		try
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM teams "); 
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Team teamObject = new Team();
				
				teamObject.setId(rs.getInt("idTeams"));
				teamObject.setName(rs.getString("Name"));
				teamObject.setPresident(rs.getString("President"));
				teamObject.setCoach(rs.getString("Coach"));
				teamObject.setStadium(rs.getString("Stadium"));
				teamObject.setFounded(rs.getString("Founded"));
				teamObject.setWiners(rs.getString("Winers"));
				teamObject.setDescription(rs.getString("Description"));
				teamObject.setUrl(rs.getString("URL"));
				if (teamObject.getName().equals(nameTeam))
				{
					teamData.add(teamObject);
				}
				
			}
			return teamData;
		}
		catch(Exception e)
		{
			System.out.println("Check DataBaseManager in GetTeam");
			throw e;
		}
	}

}
