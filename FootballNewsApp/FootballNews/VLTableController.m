//
//  VLTableController.m
//  FootballNews
//
//  Created by Vlad Valt on 6/6/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import "VLTableController.h"
#import "VLCell.h"
#import "VLDataSource.h"
#import "VLTeamData.h"
#import "VLHeaderCell.h"

@interface VLTableController () <VLDataSourceDelegate,HeaderCellDelegate>


@property (nonatomic,strong)VLDataSource * datasource;

@end

@implementation VLTableController

-(void) prepareDataSource{
    if(!_datasource){
        [self.refreshControl beginRefreshing];
        dispatch_queue_t queue = dispatch_queue_create("dataget", nil);
        dispatch_async(queue, ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
            [NSThread sleepForTimeInterval:2.0];
            _datasource = [[VLDataSource alloc] init];
            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

            _datasource.delegate=self; 

            dispatch_async(dispatch_get_main_queue(), ^{                
                [self.tableView reloadData];
                [self.refreshControl endRefreshing];
            });
        });
    }
}

-(void) ptsPressed{
    [_datasource sortByPTS];
}

-(void) clubPressed{
    [_datasource sortByClub];
}

-(void) posPressed{
    [_datasource sortByPos];
}

-(void) wPressed{
    [_datasource sortByW];
}

-(void) gdPressed{
    [_datasource sortByGD];
}

-(void) pPressed{
    [_datasource sortByP];
}

-(void) dPressed{
    [_datasource sortByD];
}


-(void) lPressed{
    [_datasource sortByL];
}

-(void) gaPressed{
    [_datasource sortByGA];
}

-(void) gfPressed{
    [_datasource sortByGF];
}



-(void) notificate{
    [self.tableView reloadData];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([sender isKindOfClass:[UITableViewCell class]]){
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if(indexPath) {
            if([segue.identifier isEqualToString:@"Show Team"]){
            if([segue.destinationViewController respondsToSelector:@selector(setInfo:)]){
                    VLTeamData * info = _datasource.teams[indexPath.row-1];
                [segue.destinationViewController performSelector:@selector(setInfo:) withObject:info];
                [segue.destinationViewController setTitle: info.team_Name];
                }
            }
        }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [_datasource getDataOnSuccess:^(){
        NSLog(@"success");
    }  onFailure:^(){
        NSLog(@"failure");
    }];
     
    [self prepareDataSource];
    [self.refreshControl addTarget:self action:@selector(prepareDataSource) forControlEvents:UIControlEventValueChanged];
    
    



    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_datasource.teams count]+1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self prepareDataSource];
    NSString *CellIdentifier = @"content";

    
    if(indexPath.row==0){
        CellIdentifier = @"header";
        VLHeaderCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        return cell;

    }
    
    VLCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    if(indexPath.row>=1){
        cell.positionLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] position]] ;
        cell.clubLabel.text=[(VLTeamData *)_datasource.teams[indexPath.row-1] team_Name];
        cell.pLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] matches]];
        cell.wLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] win]];
        cell.lLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] lose]];
        cell.dLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] standoff]];
        cell.gfLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] goals]];
        cell.gaLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] missed_Goals]];
        cell.gdLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] difference_Goals]];
        cell.ptsLabel.text=[NSString stringWithFormat:@"%d",[(VLTeamData *)_datasource.teams[indexPath.row-1] points]];
    }
        
    return cell;
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
