package com.leginevych.football.webservice;

import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.leginevych.football.DTO.Team;
import com.leginevych.football.DTO.TeamInLeague;
import com.leginevych.football.model.RequestManager;
import com.leginevych.football.transformer.LeagueTransformer;



@Path("/WebService")
public class WebService {

		@GET
		@Path("/GetLeague")
		//@Produces("MediaType.APPLICATION_JSON;charset=utf-8")
		@Produces("application/json ;charset=utf-8 ")
		public String messageLeague()
		{
			String data = null;
			try 
			{
				ArrayList<TeamInLeague> leagueData = null;
				RequestManager projectManager= new RequestManager();
				leagueData = projectManager.GetLeagueTeam();
				data=LeagueTransformer.toJSONLeague(leagueData);
			}
			catch (Exception e)
			{
				System.out.println("Exception Call GetLeague Error"); //Console 
			}
			
			return data;
		}

		
		@GET
		@Path("/GetTeam")
		//@Produces("MediaType.APPLICATION_JSON;charset=utf-8")
		@Produces ("application/json ;charset=utf-8 ")
		public String messageTeam(@QueryParam("name") String name)
		{	
			String data = null;
			try 
			{
				ArrayList<Team> teamData = null;
				RequestManager projectManager= new RequestManager();
				teamData = projectManager.GetTeam(name);
				data=LeagueTransformer.toJSONTeam(teamData);
			}
			catch (Exception e)
			{
				System.out.println("Exception Call GetTeam Error"); //Console 
			}	
			return data;
		}
		
		 @GET
		 @Path("/GetImage/{taskId}")
		 @Produces("image/png")
		 public Response getFullImage(@PathParam("taskId") final String taskId) {
		  Response response = null;
		  Image image = null;
		  File sourceimage = new File("C:/images/" + taskId);
			
		 // System.out.println(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
		  if (sourceimage.exists()) {
			  try {
				  image = ImageIO.read(sourceimage);
			  } catch (Exception e) {
				  response = Response.status(Status.NOT_MODIFIED).build();
				  e.printStackTrace();
			  }
		   response = Response.ok(image).build();
		   }
		   else {
		   response = Response.status(Status.NOT_FOUND).build();
		   }
		//  System.out.println(response.toString());
		 
		return response;
		}
		
}
