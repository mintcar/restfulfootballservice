//
//  VLHeaderCell.m
//  FootballNews
//
//  Created by Vlad Valt on 6/11/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import "VLHeaderCell.h"
#import "VLTeamData.h"



@interface VLHeaderCell ()

@property (weak, nonatomic) IBOutlet UIButton *posButton;
@property (weak, nonatomic) IBOutlet UIButton *clubButton;
@property (weak, nonatomic) IBOutlet UIButton *pButton;
@property (weak, nonatomic) IBOutlet UIButton *wButton;
@property (weak, nonatomic) IBOutlet UIButton *dButton;
@property (weak, nonatomic) IBOutlet UIButton *lButton;
@property (weak, nonatomic) IBOutlet UIButton *gfButton;
@property (weak, nonatomic) IBOutlet UIButton *gaButton;
@property (weak, nonatomic) IBOutlet UIButton *gdButton;
@property (weak, nonatomic) IBOutlet UIButton *ptsButton;



@end

@implementation VLHeaderCell

- (IBAction)ptsPressed {
    [_delegate ptsPressed];
}
- (IBAction)pPressed {
    [_delegate pPressed];
}
- (IBAction)posPressed {
    [_delegate posPressed];
}
- (IBAction)wPressed {
    [_delegate wPressed];
}
- (IBAction)dPressed {
    [_delegate dPressed];
}
- (IBAction)lPressed {
    [_delegate lPressed];
}
- (IBAction)gfPressed {
    [_delegate gfPressed];
}
- (IBAction)gaPressed {
    [_delegate gaPressed];
}
- (IBAction)gdPressed {
    [_delegate gdPressed];
}
- (IBAction)clubPressed {
    [_delegate clubPressed];
}



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
