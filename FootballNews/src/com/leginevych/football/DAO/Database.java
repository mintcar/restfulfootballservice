package com.leginevych.football.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

	public Connection Get_Connection() throws Exception
	{
		try
		{
			String connectionURL = "jdbc:mysql://localhost:3306/football";
			Connection connection = null;
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(connectionURL, "root", "senator");
			return connection;
		}
		catch (SQLException e)
		{

			System.out.println("SQL query error");	
			throw e; 
		}
		catch (Exception e)
		{
			System.out.println("Database connection error");
			throw e; 
		}
	}
}
