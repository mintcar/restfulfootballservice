//
//  VLTeamData.m
//  FootballNews
//
//  Created by Vlad Valt on 6/5/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import "VLTeamData.h"

@implementation VLTeamData

int freeID=0;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}


- (id) initWithString: (NSString *) teamName andPosition:(int) position andMatches: (int) matches andWin: (int) win andLose: (int) lose andStandoff: (int) standoff andGoals: (int) goals andMissedGoals: (int) missedGoals andDifferenceGoals: (int) differenceGoals andPoints: (int) points
{
    if (self= [super init]) {
        
        _team_ID=freeID++;
        _team_Name=teamName;
        _position=position;
        _matches=matches;
        _win=win;
        _lose=lose;
        _standoff=standoff;
        _goals=goals;
        _missed_Goals=missedGoals;
        _difference_Goals=differenceGoals;
        _points=points;
                 
    }
    return self;
         
}

- (NSComparisonResult)compareByPTS:(VLTeamData *)otherObject {
    return [[[NSNumber alloc ]initWithInt:otherObject.points] compare: [[NSNumber alloc ]initWithInt:self.points] ];
}

- (NSComparisonResult)compareByP:(VLTeamData *)otherObject {
    return [[[NSNumber alloc ]initWithInt:otherObject.matches] compare: [[NSNumber alloc ]initWithInt:self.matches] ];
}

- (NSComparisonResult)compareByPosition:(VLTeamData *)otherObject{
    return [[[NSNumber alloc ]initWithInt:self.position] compare: [[NSNumber alloc ]initWithInt:otherObject.position] ];
}

- (NSComparisonResult)compareByW:(VLTeamData *)otherObject{
    return [[[NSNumber alloc ]initWithInt:otherObject.win] compare: [[NSNumber alloc ]initWithInt:self.win] ];
}

- (NSComparisonResult)compareByD:(VLTeamData *)otherObject{
    return [[[NSNumber alloc ]initWithInt:otherObject.standoff] compare: [[NSNumber alloc ]initWithInt:self.standoff] ];
}

- (NSComparisonResult)compareByL:(VLTeamData *)otherObject{
    return [[[NSNumber alloc ]initWithInt:self.lose] compare: [[NSNumber alloc ]initWithInt:otherObject.lose] ];
}

- (NSComparisonResult)compareByGF:(VLTeamData *)otherObject{
    return [[[NSNumber alloc ]initWithInt:otherObject.goals] compare: [[NSNumber alloc ]initWithInt:self.goals] ];
}

- (NSComparisonResult)compareByGA:(VLTeamData *)otherObject{
    return [[[NSNumber alloc ]initWithInt:otherObject.missed_Goals] compare: [[NSNumber alloc ]initWithInt:self.missed_Goals] ];
}

- (NSComparisonResult)compareByGD:(VLTeamData *)otherObject{
    return [[[NSNumber alloc ]initWithInt:otherObject.difference_Goals] compare: [[NSNumber alloc ]initWithInt:self.difference_Goals] ];
}

- (NSComparisonResult)compareByClub:(VLTeamData *)otherObject{
    return [_team_Name compare:otherObject.team_Name];
}

@end
