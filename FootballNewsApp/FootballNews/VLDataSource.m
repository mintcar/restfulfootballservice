//
//  VLDataSource.m
//  FootballNews
//
//  Created by Vlad Valt on 6/10/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import "VLDataSource.h"
#import "VLTeamData.h"
#import "SBJson.h"

@interface VLDataSource()

@property (nonatomic, retain) NSMutableData *jsonData;

@end

@implementation VLDataSource

- (id)init
{
    self = [super init];
    if (self) {
        /*
        VLTeamData *team1 = [[VLTeamData alloc] initWithString:@"MU" andMatches:9 andWin:15 andLose:5 andStandoff:8 andGoals:33 andMissedGoals:1 andDifferenceGoals:9 andPoints:99];
        
        VLTeamData *team2 = [[VLTeamData alloc] initWithString:@"Arsenal" andMatches:10 andWin:10 andLose:6 andStandoff:4 andGoals:32 andMissedGoals:2 andDifferenceGoals:5 andPoints:95];
        
        VLTeamData *team3 = [[VLTeamData alloc] initWithString:@"Liverpool" andMatches:11 andWin:5 andLose:7 andStandoff:2 andGoals:22 andMissedGoals:3 andDifferenceGoals:7 andPoints:98];
        
        _teams = [NSArray arrayWithObjects:team1,team2,team3,nil];
         */
        
        [self initMyOwnJSON];
    }
    return self;
}

-(void) sortByPTS{
    
    _teams = [_teams sortedArrayUsingSelector:@selector(compareByPTS:)];
    [_delegate notificate];
}

-(void) sortByP{
    _teams = [_teams sortedArrayUsingSelector:@selector(compareByP:)];
    [_delegate notificate];
}

-(void) sortByPos{
    _teams = [_teams sortedArrayUsingSelector:@selector(compareByPosition:)];
    [_delegate notificate];
}

-(void) sortByW{
    _teams =[_teams sortedArrayUsingSelector:@selector(compareByW:)];
    [_delegate notificate];
}

-(void) sortByD{
    _teams =[_teams sortedArrayUsingSelector:@selector(compareByD:)];
    [_delegate notificate]; 
}

-(void) sortByL{
    _teams =[_teams sortedArrayUsingSelector:@selector(compareByL:)];
    [_delegate notificate];
}

-(void) sortByGF{
    _teams =[_teams sortedArrayUsingSelector:@selector(compareByGF:)];
    [_delegate notificate];
}

-(void) sortByGA{
    _teams =[_teams sortedArrayUsingSelector:@selector(compareByGA:)];
    [_delegate notificate];
}

-(void) sortByGD{
    _teams =[_teams sortedArrayUsingSelector:@selector(compareByGD:)];
    [_delegate notificate];
}

-(void) sortByClub{
    _teams =[_teams sortedArrayUsingSelector:@selector(compareByClub:)];
    [_delegate notificate];
}


- (void)initMyOwnJSON {
    
    NSURL *url = [NSURL URLWithString: @"http://localhost:8080/FootballNews/rest/WebService/GetLeague" ];
    [self parseJSONWithURL:url];
}

- (void) parseJSONWithURL:(NSURL *) jsonURL
{
    
        NSError *error = nil;
        // Request the data and store in a string.
            NSString *json = [NSString stringWithContentsOfURL:jsonURL
                                                  encoding:NSUTF8StringEncoding
                                                     error:&error];

            NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];

            NSArray * jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
            if(error == nil){
                NSMutableArray * arr = [[NSMutableArray alloc] init];
                for(int i=0; i<[jsonDict count]; ++i){
                    NSDictionary *dict = [jsonDict objectAtIndex:i];
                    NSString *name = [dict objectForKey: @"nameTeam"];
                    int points = [[dict objectForKey:@"points"] integerValue];
                    int matches = [[dict objectForKey:@"games"] integerValue];
                    int win = [[dict objectForKey:@"wins"] integerValue];
                    int draws = [[dict objectForKey:@"draws"] integerValue];
                    int lose = [[dict objectForKey:@"losses"] integerValue];
                    int goals = [[dict objectForKey:@"goals_scored"] integerValue];
                    int ga = [[dict objectForKey:@"goals_conceded"] integerValue];
                    int gf = [[dict objectForKey:@"goals_difference"] integerValue];
                    int position = [[dict objectForKey:@"position"] integerValue];

                    VLTeamData *team = [[VLTeamData alloc] initWithString:name andPosition: position andMatches:matches andWin:win andLose:lose andStandoff:draws andGoals:goals andMissedGoals:ga andDifferenceGoals:gf andPoints:points];
                    [arr addObject:team];
                }
                _teams = [arr copy];
                
            }
        
            // Request Failed, display error as alert.
            else
            {
                // catch
            }

}


-(NSArray *) getDataOnSuccess: (block) succes onFailure: (block) failure{
    
    NSLog(@"fff");
    succes();
    return nil;
    
}



@end
