//
//  VLCell.h
//  FootballNews
//
//  Created by Vlad Valt on 6/7/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *clubLabel;
@property (weak, nonatomic) IBOutlet UILabel *pLabel;
@property (weak, nonatomic) IBOutlet UILabel *wLabel;
@property (weak, nonatomic) IBOutlet UILabel *dLabel;
@property (weak, nonatomic) IBOutlet UILabel *lLabel;
@property (weak, nonatomic) IBOutlet UILabel *gfLabel;
@property (weak, nonatomic) IBOutlet UILabel *gaLabel;
@property (weak, nonatomic) IBOutlet UILabel *gdLabel;
@property (weak, nonatomic) IBOutlet UILabel *ptsLabel;

@end
