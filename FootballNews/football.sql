CREATE DATABASE  IF NOT EXISTS `football` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `football`;
-- MySQL dump 10.13  Distrib 5.6.12, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: football
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barclayspremierleague`
--

DROP TABLE IF EXISTS `barclayspremierleague`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barclayspremierleague` (
  `idBarclaysPremierLeague` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `games` int(11) NOT NULL,
  `wins` int(11) NOT NULL,
  `draws` int(11) NOT NULL,
  `losses` int(11) NOT NULL,
  `goals_scored` int(11) NOT NULL,
  `goals_conceded` int(11) NOT NULL,
  `goal_difference` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`idBarclaysPremierLeague`),
  UNIQUE KEY `idBarclaysPremierLeague_UNIQUE` (`idBarclaysPremierLeague`),
  UNIQUE KEY `position_UNIQUE` (`position`),
  KEY `name_idx` (`name`),
  CONSTRAINT `name` FOREIGN KEY (`name`) REFERENCES `teams` (`Name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barclayspremierleague`
--

LOCK TABLES `barclayspremierleague` WRITE;
/*!40000 ALTER TABLE `barclayspremierleague` DISABLE KEYS */;
INSERT INTO `barclayspremierleague` VALUES (1,1,'Манчестер Юнайтед',38,28,5,5,86,43,43,89),(2,2,'Манчестер Сити',38,23,9,6,66,34,32,78),(3,3,'Челси',38,22,9,7,75,39,36,75),(4,4,'Арсенал',38,21,10,7,72,37,35,73),(5,5,'Тоттенхэм',38,21,9,8,66,46,20,72),(6,6,'Эвертон',38,16,15,7,55,40,15,63),(7,7,'Ливерпуль',38,16,13,9,71,43,28,61),(8,8,'Вест Бромвич',38,14,7,17,53,57,-4,49),(9,9,'Суонси',38,11,13,14,47,51,-4,46),(10,10,'Вест Хэм',38,12,10,16,45,53,-8,46),(11,11,'Норвич',38,10,14,14,41,58,-17,44),(12,12,'Фулхэм',38,11,10,17,50,60,-10,43),(13,13,'Сток Сити',38,9,15,14,34,45,-11,42),(14,14,'Саутгемптон',38,9,14,15,49,60,-11,41),(15,15,'Астон Вилла',38,10,11,17,47,69,-22,41),(16,16,'Ньюкасл',38,11,8,19,45,68,-23,41),(17,17,'Сандерленд',38,9,12,17,41,54,-13,39),(18,18,'Уиган',38,9,9,20,47,73,-26,36),(19,19,'Рэдинг',38,6,10,22,43,73,-30,28),(20,20,'КПР',38,4,13,21,30,60,-20,25);
/*!40000 ALTER TABLE `barclayspremierleague` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `idTeams` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) NOT NULL,
  `President` varchar(200) DEFAULT NULL,
  `Coach` varchar(200) DEFAULT NULL,
  `Stadium` varchar(200) DEFAULT NULL,
  `Founded` int(11) DEFAULT NULL,
  `Winers` longtext,
  `Description` longtext,
  `URL` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idTeams`),
  UNIQUE KEY `idTeams_UNIQUE` (`idTeams`),
  UNIQUE KEY `Name_UNIQUE` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'Манчестер Юнайтед','I','I','Olimpic Stadium',1976,'Last year - nothing','«Манче́стер Юна́йтед» (англ. Manchester United Football Club) — англійський професійний футбольний клуб — заснований як Ньютон Гіт 1878 року. Клуб змінив свою назву на «Манчестер Юнайтед» 1902 року і переїхав на «Олд Траффорд» в 1910 році.','manchester_united.png'),(2,'Манчестер Сити','I','I','Stan',1999,'Not','Клуб засновано в 1880 році під назвою «Вест Гордон Сент Маркс» (англ. West Gordon Saint Marks). У 1887 році клуб переїхав на новий стадіон і змінив назву на «Ардвік»[2]. Фінансові проблеми в англійському футболі в 1893-84 роках призвели до реорганізації клубу і він отримав свою сучасну назву.','manchester_city.png'),(3,'Челси',NULL,NULL,NULL,NULL,'','«Че́лсі» (англ. Chelsea F.C.; [ˈtʃɛlsiː]) — професійний англійський футбольний клуб із західного Лондона. Заснований 1905 року, виступає в англійській Прем\'єр-лізі і провів більшу частину своєї історії у вищому дивізіоні англійського футболу. Один з клубів-засновників англійської Прем\'єр-ліги 1992 року. «Челсі» чотири рази ставав чемпіоном Англії, шість разів вигравав кубок Англії і чотири рази Кубок Футбольної Ліги. Також клуб добився успіху на європейській арені, виграв Лігу чемпіонів, двічі Кубок володарів кубків УЄФА та Суперкубок УЄФА.','chelsea.png'),(4,'Арсенал',NULL,NULL,NULL,NULL,NULL,'Футбольний Клуб «Арсенал» (англ. Arsenal Football Club), або просто Арсенал (англ. Arsenal) — англійський футбольний клуб з північного Лондона, є одним з найуспішніших клубів в англійському футболі. 13 разів ставав чемпіоном Англії, поступаючись за цим показником лише «Ліверпулю» і «Манчестер Юнайтед».','arsenal.png'),(5,'Тоттенхэм',NULL,NULL,NULL,NULL,NULL,'То́ттенгем Го́тспур (англ. Tottenham Hotspur FC) — професійний англійський футбольний клуб, виступає в англійській Прем\'єр-лізі. Заснований — 1882 року. Клуб зазвичай називають \"шпори\"(spurs). Стадіон Вайт Гарт Лейн розташовани в північній частині Лондона, в районі Харінгей. За свою більш ніж столітню історію клуб став одним з найтитулованіших на Туманному Альбіоні. Девіз клубу — Audere est Facere («Зважитися — означає зробити»).','tottenham.png'),(6,'Эвертон',NULL,NULL,NULL,NULL,NULL,'Е́вертон (англ. Everton Football Club) — професіональний англійський футбольний клуб з міста Ліверпуль. У вищій лізі англійського футболу «іриски» провели 110 сезонів, що є рекордним показником для всіх команд чемпіонату. «Евертон» дев\'ять разів перемагав у найвищому дивізіоні, що є четвертим результатом серед всіх переможців змагання. Клуб засновано у 1878 священиками церкви Святого Домінґо, на честь якого клуб отримав свою першу назву, але вже незабаром клуб перейменували в «Евертон», в честь району Ліверпуля, в якому знаходилась церква.','everton.png'),(7,'Ливерпуль',NULL,NULL,NULL,NULL,NULL,'Футбо́льний клуб «Ліверпу́ль» (англ. Liverpool Football Club) — англійська професіональна футбольна команда з Ліверпуля. Засновано 15 березня 1892. Команда грає в Прем\'єр-лізі. «Ліверпуль» виграв 18 чемпіонатів Англії (другий після «Манчестер Юнайтед» результат). Клубу також належить англійський рекорд за кількістю виграних європейських титулів.','liverpool.png'),(8,'Вест Бромвич',NULL,NULL,NULL,NULL,NULL,'«Вест-Бромвіч Альбіон» (англ. «West Bromwich Albion Football Club»; також відомий як «West Brom», «The Baggies», «Albion», «The Throstles», «W.B.A.») — англійський футбольний клуб з міста Вест-Бромвіч. Утворений в 1878 році. Домашні матчі проводить на стадіоні «Готорнс». Кольори клубу — синьо-білі. Клуб засновано в 1878 році робітниками Західного Бромвіча з Salter’s Spring Works. В 1888 році став одним з клубів-засновників Футбольної ліги Англії. З 1900 року проводить свої домашні матчі на стадіоні Готорнс. Майже всю свою історію клуб грає в смугастій синьо-білій формі.','bromwich.png'),(9,'Суонси',NULL,NULL,NULL,NULL,NULL,'«Свонсі Сіті» (валл. Clwb Pêl-droed Dinas Abertawe, англ. Swansea City Association Football Club) — валлійський професійний футбольний клуб із міста Свонсі, що виступає в системі англійських футбольних ліг. Клуб заснований у 1912 році під назвою Свонсі Таун і в першому ж році існування виграв Кубок Уельсу. 1920 року приєднався до футбольної ліги Англії, та з того часу є її незмінним учасником, в той же час продовжуючи виступати і в Кубку Уельсу. Найвищими досягненнями Свонсі були півфінали Кубку Англії у 1926 та 1964 роках. 1961 року, вигравши Кубок Уельсу, команда вперше в історії взяла участь в єврокубках, програвши в першому ж раунді кубка володарів кубків «Мотор Єні» з НДР (2-2, 1-5).','swansea.gif'),(10,'Вест Хэм',NULL,NULL,NULL,NULL,NULL,'Вест Гем Юна́йтед (англ. West Ham United F.C.) — англійський професіональний футбольний клуб зі Східного Лондона. Домашньою ареною для клубу з 1904 року є «Болейн Граунд», відоміший як «Аптон Парк». Заснований у 1895 році під назвою «Темз Айронворкс». У 1900 році назву змінили на сучасну. Спочатку «молоти» грали у Південній та Західній Лігах, а у 1919 році приєдналися до Футбольної Ліги. У 1923 році пройшли до фіналу Кубка Англії. У фінальному матчі були обіграні «Болтоном». У 1932 році «Вест Гем» вибув з Першого дивізіону.','west_ham_united.png'),(11,'Норвич',NULL,NULL,NULL,NULL,NULL,'Но́рвіч Сі́ті (англ. Norwich City Football Club) — англійський футбольний клуб із міста Норвіч, заснований 1902 року. Традиційні кольори клубу — жовто-зелені, звідси й походить їх прізвисько — «канарки» (Canaries). На клубній емблемі зображена канарка, що стоїть верхи на м\'ячі. З 1935 року проводить матчі на стадіоні «Керроу Роуд». Володар Кубка Ліги 1962 і 1985. Принциповим суперником «Норвіча» вважається «Іпсвіч Таун». Клуб був утворений в 1902 році і спочатку грав на «Ньюмаркет Роуд». У 1908 році клуб переїхав на нову арену на «Розарі Роуд», яка стала відомою як «Нест» (гніздо). До 1930х років місткість стадіону виявилася недостатньою для зростаючого натовпу уболівальників, і в 1935 році клуб переїхав на свій нинішній стадіон — «Керроу Роуд».','norvych.png'),(12,'Фулхэм',NULL,NULL,NULL,NULL,NULL,'«Фу́лгем» (англ. Fulham Football Club) — англійський футбольний клуб з Лондона. Названий за іменем лондонського району Фулем, в якому розміщений домашній стадіон команди «Крейвен Коттедж», відкритий у 1896 році. Клуб заснований у 1879 році. Прийнятий у другий дивізіон Футбольної ліги 1 червня 1905 року. За всю свою історію «Фулгем» жодного разу не ставав чемпіоном. Клуб вважається типовим середняком, який часто бореться за виживання. У 1968 році клуб залишив Перший дивізіон. Почалася мандрівка клубу по різних лігах. У 1997 році клуб придбав міильярдер Мохаммед Аль-Файєд. Тренером був запрошений відомий у минулому французький футболіст Жан Тігана, який змінив Кевіна Кігана. Тігана вивів «Фулгем» в Прем\'єр-лігу, але досягнути більшого не зумів. Після того, як Тігана залишив клуб, багатьом стало зрозуміло, що стати чемпіоном «Фулгему» судилося не швидко. Багато фахівців скептично зустріли появу на тренерському мостику вчорашнього капітана команди Кріса Колмана, мало хто вірив, що молодий тренер зможе влити нову кров. При Колмані команда почала грати в живіший футбол, але результату, тобто трофеїв не було, і знову звинуватили тренера, що відсутність досвіду не дозволяє команді займати високі місця. Колмана змінив Ларі Санчес, який працював зі збірною Північної Ірландії і під його керівництвом збірна досягла хороших результатів. Але Санчес не зміг нічого зробити в клубі, який почав котитися вниз. Прихід в команду Роя Ходжсона також не додав радості, «Фулгем» був одним із аутсайдерів. Лише чудо врятувало команду. В сезоні 2007/2008 клуб вів боротьбу з «Редінгом», і залишився в еліті тільки через кращу різницю забитих і пропущених голів. Але у сезоні 2009/2010 Рой Ходжсон реабілітувався, і зараз команда знаходиться у середині турнірної таблиці. Крім того, він зумів вперше за історію клубу вивести його у фінал Ліги Європи де «Фулгем» зіграв з іспанським «Атлетіко».','fullham.png'),(13,'Сток Сити',NULL,NULL,NULL,NULL,NULL,'Сток Сіті (англ. Stoke City Football Club) — професійний англійський футбольний клуб, розташований в місті Сток-он-Трент, графство Стаффордшир. Виступає в англійській Прем\'єр-лізі. Є другим найстарішим професійним клубом Англії після «Ноттс Каунті». «Сток Сіті» належить до «старшої вікової категорії» британських футбольних клубів: з існуючих нині клубів старіший від нього лише «Ноттс Каунті». Немає єдності думок щодо точної дати створення клубу. Достовірно відомо, що випускники школи Чартерхаус заснували футбольний клуб у 1863 році, але відомостей про матчі, що проводилися в ті роки, не збереглося. У 1868 році в місцевій пресі з\'явилися повідомлення про клуб, створений випускником школи Чартерхаус Генрі Амондом. Амонд забив перший гол в історії команди, але незабаром пішов з клубу, вибравши кар\'єру інженера-будівельника.','stoke_city.png'),(14,'Саутгемптон',NULL,NULL,NULL,NULL,NULL,'Саутгемптон (англ. Southampton Football Club) — футбольний клуб з міста Саутгемптон (Гемпшир, Англія). Заснований у 1885 році. Грає у червоно-білих смугастих футболках. Стадіон — «Сент-Меріс Стедіум» міскістю 32551 глядачів. За підсумками сезону 2008/09 клуб вилетів з Чемпіонату до Ліги 1. Початок шляху 21 листопада 1885 року команда, складена з членів Молодіжної Асоціації церкви Святої Марії, на території Гемпширского крикетного клубу розгромила клуб «Freemantle FC» з рахунком 5:1. Саме з цієї дати веде свій відлік історія футбольного клубу «Саутгемптон». Оскільки засновниками клубу були члени Молодіжної Асоціації церкви Святої Марії, звідси і пішло знамените прізвисько «Саутгемптона» — «Святі». Спочатку команда носила назву «Saint Mary\'s Young Men\'s Association FC». В основу тієї першої команди входили викладачі місцевих шкіл. Тим часом у грудні 2005 року головним тренером «Саутгемптона» стає Джордж Берлі. Чемпіонат Футбольної Ліги «Святі» завершили в ролі середнячка, зайнявши 12 місце. Сезон 2006/07 видався вдалішим — в команду прийшли такі перспективні гравці, як поляки Гжегош Расяк і Марек Сагановскі, а також доморощений 17-річний Гарет Бейл. «Саутгемптону» вдалося зайняти шосту позицію, що дозволяло їм взяти участь у стикових матчах за вихід у Прем\'єр-Лігу. Проте в драматичній кінцівці півфінального матчу в серії пенальті щасливішими виявилися футболісти «Дербі Каунті». Наступний же рік і зовсім поставив клуб на межу фінансового і турнірного краху — «Саутгемптон» зміг убезпечити себе від колись небаченого вильоту в Лігу 1 тільки в останньому турі чемпіонату, обігравши «Шеффілд Юнайтед». Наступний сезон 2008/09 «Святі» почали в експериментальному молодіжному складі і весь чемпіонат були на межі вильоту. Але були проблеми і серйозніше — в кінці березня материнська компанія клубу «Southampton Leisure Holding» збанкрутувала, в результаті чого, за результатами детального аналізу ситуації в клубі, Футбольна Ліга прийняла рішення зняти з футбольного клубу «Саутгемптон» 10 очок. Це означало офіційний виліт клубу в Лігу 1. Більше того, оскільки команда і так вилітала за спортивними принципами наступний чемпіонат у Лізі 1 «Саутгемптон» почав з показником «-10 очок». Таким чином, вперше з 1960 року «Святі» потрапили у третій за рівнем ешелон англійського чемпіонату. Єдина радість для вболівальників прийшла в середині липня 2009 року. Клуб був придбаний швейцарським мільярдером німецького походження Маркусом Лібхером, яким була проголошена стратегія на поступове повернення «Саутгемптона» в еліту англійського футболу. ','south.jpg'),(15,'Астон Вилла',NULL,NULL,NULL,NULL,NULL,'«Астон Вілла» (англ. Aston Villa) — професіональний англійський футбольний клуб з міста Бірмінгем, який грає у Англійській Прем\'єр-Лізі. Заснований у 1874 році, і грає на своєму полі Вілла Парк з 1897 року. Клуб був одним з засновників Футбольної ліги у 1888 році та Прем\'єр-Ліги у 1992 році. ','astom_villa.png'),(16,'Ньюкасл',NULL,NULL,NULL,NULL,NULL,'Ньюкасл Юнайтед (англ. Newcastle United Football Club) — англійський професіональний футбольний клуб з міста Ньюкасл-апон-Тайн. Виступає в англійській Прем\'єр-Лізі.Чемпион Англии - 1905, 1907, 1909, 1927','newcastle.png'),(17,'Сандерленд',NULL,NULL,NULL,NULL,NULL,'«Са́ндерленд» (англ. Sunderland Association Football Club) — професіональний футбольний клуб із Сандерленд, що на півночі Англії. Заснований 1887 року. У 1892, 1893, 1895, 1902, 1913 та 1936 клуб здобував титул чемпіона Англії. Загальновідомий «нік» 6-кратних чемпіонів Англії — «Чорні коти». І хоча ці самі коти (або кішки, кому як подобається) ось уже практично 200 років тісно переплітаються з історією міста Сандерленд і його однойменної команди, офіційний статус вони отримали зовсім недавно. А, точніше сказати, в 1997 році. До цієї дати «Сандерленд» виступав на старенькому «Рокер Парку». Власне назва стадіону і служила джерелом прізвиськ — від ласкавих «Рокеритів» до серйозних «Рокерів». Проте з переїздом на «Стедіум оф Лайт» все це стало якось неактуально. Треба було що-небудь інше. І керівництво закликало на допомогу фанатів. У результаті практично половина тих, хто голосував віддала перевагу варіанту «чорні коти». І це, повірте, відбулося не випадково.','sunderland.png'),(18,'Уиган',NULL,NULL,NULL,NULL,NULL,'Ві́ган Атле́тик (англ. Wigan Athletic) — професіональний англійський футбольний клуб з міста Віган. Заснований у 1932 році і зараз є наймолодшим клубом у Прем\'єр-Лізі.[3] До 1932 року у Вігані, який славетний своїми досягеннями у регбіліг, розпалися п\'ять футбольних команд.[4] «Атлетик» потрапив до системи футбольних ліг лише через 46 років свого існування.[5] Клуб виступає у вищій лізі англійського футболу з 2005 року.','wigan.png'),(19,'Рэдинг',NULL,NULL,NULL,NULL,NULL,'Редінг (англ. Reading FC) — англійський футбольний клуб із міста Редінг. Неофіційна назва клубу серед уболівальників — «роялс». Президент: Джон Мадейски','reading.gif'),(20,'КПР',NULL,NULL,NULL,NULL,NULL,'Президент: Тони Фернандес. Главный тренер: Харри Реднапп. Стадион: Loftus Road. Год основания: 1882.','kpr.png');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-16 20:05:12
