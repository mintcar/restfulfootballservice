//
//  VLViewController.m
//  FootballNews
//
//  Created by Vlad Valt on 6/4/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//
#import "VLTeamData.h"
#import "VLViewController.h"

@interface VLViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *image;
@property (strong,nonatomic) UIImageView * imageView;
@property (weak, nonatomic) IBOutlet UITextView *teamInfo;
@property NSString *imgURL;

@property (strong, nonatomic) NSString * description;

@end

@implementation VLViewController


-(void) setInfo:(VLTeamData *)info{
    _info=info;
    [self initMyOwnJSON];
}

-(void) resetImage{
    if(self.image){
        NSURL *imageURL = [[NSURL alloc] initWithString:@"http://www.bubblews.com/assets/images/news/1457110047_1369647405.jpg"];
        if(_info){
            _imgURL = [@"http://localhost:8080/FootballNews/rest/WebService/GetImage/" stringByAppendingString:_imgURL];
            imageURL = [[NSURL alloc] initWithString:_imgURL];
        }
        _image.contentSize=CGSizeZero;
        _imageView=nil;
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:imageURL];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        if(image){
            self.image.contentSize = image.size;
            self.imageView.image = image;
            self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
        }
    }
}

-(UIImageView *) imageView{
    if(!_imageView){
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    }
    return _imageView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self resetImage];
    [self.image addSubview:self.imageView];
    
    if(_description)
        _teamInfo.text = _description;
}


- (void)initMyOwnJSON {
    
    // encode for kirilic characters to url
    NSString * encodedString =
    (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                        NULL,
                                                        (CFStringRef)_info.team_Name,
                                                        NULL,
                                                        (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                        kCFStringEncodingUTF8 ));
    
    NSString * strUrl = [@"http://localhost:8080/FootballNews/rest/WebService/GetTeam?name=" stringByAppendingString:encodedString];

    NSURL *url = [NSURL URLWithString: strUrl];
    
    [self parseJSONWithURL:url];
}

- (void) parseJSONWithURL:(NSURL *) jsonURL
{
    
    NSError *error = nil;

        NSString *json = [NSString stringWithContentsOfURL:jsonURL
                                                  encoding:NSUTF8StringEncoding
                                                     error:&error];    
        
        NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray * jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        if(error == nil){
            NSDictionary *dict = [jsonDict objectAtIndex:0];
            NSString *name = [dict objectForKey: @"description"];
            NSString *imageURLStr = [dict objectForKey: @"url"];
            _imgURL = imageURLStr;
            _description =name;
        }
                    
} 

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

@end
