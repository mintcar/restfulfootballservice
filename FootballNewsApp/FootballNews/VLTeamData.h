//
//  VLTeamData.h
//  FootballNews
//
//  Created by Vlad Valt on 6/5/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLTeamData : NSObject

- (id) initWithString: (NSString *) teamName andPosition:(int) position andMatches: (int) matches andWin: (int) win andLose: (int) lose andStandoff: (int) standoff andGoals: (int) goals andMissedGoals: (int) missedGoals andDifferenceGoals: (int) differenceGoals andPoints: (int) points;

@property (nonatomic) int team_ID;
@property (nonatomic) NSString *team_Name;

@property (nonatomic) int matches;
@property (nonatomic) int win;
@property (nonatomic) int standoff;
@property (nonatomic) int lose;
@property (nonatomic) int goals;
@property (nonatomic) int missed_Goals;
@property (nonatomic) int difference_Goals;
@property (nonatomic) int points;
@property (nonatomic) int position;

- (NSComparisonResult)compareByPosition:(VLTeamData *)otherObject;
- (NSComparisonResult)compareByW:(VLTeamData *)otherObject;
- (NSComparisonResult)compareByD:(VLTeamData *)otherObject;
- (NSComparisonResult)compareByL:(VLTeamData *)otherObject;
- (NSComparisonResult)compareByGF:(VLTeamData *)otherObject;
- (NSComparisonResult)compareByGA:(VLTeamData *)otherObject;
- (NSComparisonResult)compareByGD:(VLTeamData *)otherObject;
- (NSComparisonResult)compareByClub:(VLTeamData *)otherObject;



@end
