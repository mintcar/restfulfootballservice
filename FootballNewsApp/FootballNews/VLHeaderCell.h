//
//  VLHeaderCell.h
//  FootballNews
//
//  Created by Vlad Valt on 6/11/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HeaderCellDelegate <NSObject>

-(void) ptsPressed;
- (void)pPressed;
- (void)posPressed;
- (void)wPressed;
- (void)dPressed;
- (void)lPressed;
- (void)gfPressed;
- (void)gaPressed;
- (void)gdPressed;
- (void)clubPressed;

@end

@interface VLHeaderCell : UITableViewCell

@property id<HeaderCellDelegate> delegate;

@end
