package com.leginevych.football.DTO;

public class Team {

	private int id;
	private String name;
	private String president;
	private String coach;
	private String stadium;
	private String founded;
	private String winers;
	private String description;
	private String url;


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPresident() {
		return president;
	}
	public void setPresident(String president) {
		this.president = president;
	}
	public String getCoach() {
		return coach;
	}
	public void setCoach(String coach) {
		this.coach = coach;
	}
	public String getStadium() {
		return stadium;
	}
	public void setStadium(String stadium) {
		this.stadium = stadium;
	}
	public String getFounded() {
		return founded;
	}
	public void setFounded(String founded) {
		this.founded = founded;
	}
	public String getWiners() {
		return winers;
	}
	public void setWiners(String winers) {
		this.winers = winers;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getId() {
		return id;
	}
	public void setId( int id) {
		 this.id = id;
	}

}
