//
//  VLDataSource.h
//  FootballNews
//
//  Created by Vlad Valt on 6/10/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^block)() ;


@protocol VLDataSourceDelegate <NSObject>

-(void) notificate;

@end

@interface VLDataSource : NSObject

@property(nonatomic,readonly,strong) NSArray *teams;
@property (weak,nonatomic) id<VLDataSourceDelegate> delegate;

-(void) sortByPTS;
-(void) sortByP;
-(void) sortByPos;
-(void) sortByW;
-(void) sortByD;
-(void) sortByL;
-(void) sortByGF;
-(void) sortByGA;
-(void) sortByGD;

-(void) sortByClub;

-(NSArray *) getDataOnSuccess: (block) succes onFailure: (block) failure;


@end
