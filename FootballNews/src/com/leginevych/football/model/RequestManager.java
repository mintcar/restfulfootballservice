package com.leginevych.football.model;

import java.sql.Connection;
import java.util.ArrayList;

import com.leginevych.football.DTO.TeamInLeague;
import com.leginevych.football.DTO.Team;
import com.leginevych.football.DAO.Database;
import com.leginevych.football.DAO.DataBaseManager;

public class RequestManager {

	public ArrayList<TeamInLeague> GetLeagueTeam()throws Exception {
		ArrayList<TeamInLeague> data = null;
		try {
		Database database= new Database();
		Connection connection = database.Get_Connection();
		DataBaseManager project= new DataBaseManager();
		data =project.GetLeague(connection);
		}
		catch (Exception e) {
			System.out.println("Check RequestManager in GetLeagueTeam");
		throw e;
		}
		return data;
		}

	public ArrayList<Team> GetTeam(String name)throws Exception {
		ArrayList<Team> data = null;
		try {
		Database database= new Database();
		Connection connection = database.Get_Connection();
		DataBaseManager project= new DataBaseManager();
		data =project.GetTeam(connection, name);
		}
		catch (Exception e) {
			System.out.println("Check RequestManager in GetTeam");
		throw e;
		}
		return data;
		}
}
